package com.luminescent.digital.service;

import com.luminescent.digital.model.Product;

public interface ProductService {

	Iterable<Product> getProducts();
	Product getProductForId(Long id);
	Product save(Product product);
}
